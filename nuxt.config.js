const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: 'BPederencino', //pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'author', content: 'Bastien Pederencino' },
      { hid: 'description', name: 'description', content: pkg.description },
      {
        name: 'keywords',
        content: 'Portfolio, Fron-end, developer, Nuxt, Vue.js, Node.js'
      },

      { property: 'og:url', content: 'https://bpederencino.gitlab.io' },
      { property: 'og:type', content: 'website' },
      { property: 'og:title', content: 'Développeur Front-End Junior' },
      {
        property: 'og:image',
        content: 'https://bpederencino.gitlab.io/bpederencino.png'
      },
      {
        property: 'og:description',
        content:
          "En recherche d'un stage de quatre mois. Mon terrain de jeu se compose actuellement de Vue.js, Node.js, CSS, Sass et GitLab."
      },
      { property: 'og:site_name', content: 'BPederencino' },
      { property: 'og:locale', content: 'fr_FR' },
      { property: 'article:author', content: 'Bastien Pederencino' },

      { property: 'twitter:card', content: 'summary' },
      { property: 'twitter:site', content: '@BPederencino' },
      { property: 'twitter:creator', content: '@BPederencino' },
      { property: 'twitter:url', content: 'https://bpederencino.gitlab.io' },
      { property: 'twitter:title', content: 'Développeur Front-End Junior' },
      {
        property: 'twitter:description',
        content:
          "En recherche d'un stage de quatre mois. Mon terrain de jeu se compose actuellement de Vue.js, Node.js, CSS, Sass et GitLab."
      },
      {
        property: 'twitter:image',
        content: 'https://bpederencino.gitlab.io/bpederencino.png'
      },

      { name: 'application-name', content: 'BPederencino' },
      { name: 'msapplication-square70x70logo', content: '' },
      { name: 'msapplication-square150x150logo', content: '' },
      { name: 'msapplication-wide310x150logo', content: '' },
      { name: 'msapplication-square310x310logo', content: '' },
      { name: 'msapplication-TileColor', content: '#202126' },

      { name: 'robots', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'apple-touch-icon', href: '/favicon.ico' },
      { rel: 'icon', type: 'image/png', href: '/favicon.png' },
      { rel: 'author', href: 'humans.txt' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public'
  },

  /*
  ** Customize the base url
  */
  router: {
    base: '/'
  },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [],

  /*
  ** Nuxt.js modules
  */
  modules: [
    ,
    // Doc: https://buefy.github.io/#/documentation
    'nuxt-buefy'
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
