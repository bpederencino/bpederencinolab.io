# Bastien Pederencino&#39;s Portfolio

> I would like to share with you my skills about web development because I am looking for a **4-month internship as Junior Front-end Developer** (with a start on April 15th 2019).

You can find this website here: [bpederencino.gitlab.io](https://bpederencino.gitlab.io).

Please, use the form on my website to contact me.

## Getting started

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

## About

This project uses several **technologies**:

- [Nuxt](https://nuxtjs.org), a Framework to develop Universal Vue.js Applications.
- [Vue.js](https://vuejs.org/), a Progressive
  JavaScript Framework.
- [Buefy](https://buefy.github.io), Lightweight UI components for Vue.js based on Bulma.
- [Bulma](https://bulma.io), CSS framework based on Flexbox.
- [GitLab](https://gitlab.com/), a single application with features for the entire DevOps lifecycle.

I am proud of my **inspirations** 

- Thanks to [Daniel Fernandez Rabal](https://danielfr.com/) and his great website!
- Thanks to [Pantone](https://www.pantone.com/) which gives me color ideas.
